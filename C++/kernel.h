// avoid definition redundancies

#ifndef KERNEL_CUH_
#define KERNEL_CUH_

void gpu_blas_mmul(double *A, double *B, double*C, int m, int n, int k);
void print_matrix(const double *A, int nr_rows_A, int nr_cols_A);
void print_row_matrix(const double *A, int nr_rows_A, int nr_cols_A);

void computeP1KernelHelper(double *x, double *y, double* P, int m, int s, int d, double k, double* column_sum, double* column_sum_array);

//void GPU_fill_rand(double *A, int nr_rows_A, int nr_cols_A);
//void computeP1(int j, double* arrayX, double* arrayY, double* arrayP, int m, int s, int d, int yCols, int pRows, int pCols, double k, double* column_sum);
//passs in array columnsum //size s //size m //array x and array y // array P //value k 
//return column_sum 
//return P 


#endif