#include "io_utils.h"
#include <sys/stat.h>
#include <iostream>
#include <vnl/vnl_matrix.h>

int main( int argc, char *argv[] ) {

    struct stat sb;
    std::string fname;
    if (stat(argv[1], &sb) == 0) {
        fname = fname.assign(argv[1]);
    }
    else
    {
        std::cout<<"Invalid input" <<std::endl;
        return 1;
    }

    std::cout << "Loading " <<fname  << std::endl;

	vnl_matrix<double> model_;
	vtkSmartPointer<vtkPolyData> myPolyData;
	gmmreg::LoadStlFromFile(fname.c_str(), model_, myPolyData);
	model_.print(std::cout);
	myPolyData->Print(std::cout);
	std::string stlFilename = std::string(fname)+std::string(".stl");
	std::cout << "Writing " <<stlFilename  << std::endl;
	gmmreg::SaveStlToFile(stlFilename.c_str() ,model_,myPolyData);

}