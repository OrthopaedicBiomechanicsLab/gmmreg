#ifndef GMMREG_UTILS_IO_UTILS_H_
#define GMMREG_UTILS_IO_UTILS_H_

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include <vcl_iostream.h>
#include <vnl/vnl_vector.h>
#include <vnl/vnl_matrix.h>

#include <vtkSTLReader.h>
#include <vtkSTLWriter.h>
#include "vtkPolyData.h"
#include <vtkSmartPointer.h>

namespace gmmreg {

template<typename T>
int LoadMatrixFromTxt(const char* filename, vnl_matrix<T>& matrix);

template<typename T>
void SaveMatrixToAsciiFile(const char * filename, const vnl_matrix<T>& x);



//template<typename T>
//int SaveStlToFile(const char * filename, vnl_matrix<T>& matrix, vtkSmartPointer<vtkPolyData> polyData);

template<typename T>
void SaveVectorToAsciiFile(const char * filename, const vnl_vector<T>& x);

template<typename T>
int LoadMatrixFromTxt(const char* filename, vnl_matrix<T>& matrix) {
  std::ifstream infile(filename, std::ios_base::in);
  if (infile.is_open()) {
    if (matrix.read_ascii(infile)) {
      return matrix.rows();
    } else {
      std::cerr << "unable to parse input file " << filename
                << " as a matrix." << std::endl;
      return -1;
    }
  } else {
    std::cerr << "unable to open model file " << filename << std::endl;
    return -1;
  }
}


template<typename T>
int LoadStlFromFile(const char * filename, vnl_matrix<T>& matrix, vtkSmartPointer<vtkPolyData>& polyData) {
	vtkSmartPointer<vtkSTLReader> reader = vtkSmartPointer<vtkSTLReader>::New();
    reader->SetFileName(filename);
    reader->Update();

    polyData = reader->GetOutput();
	
    //#vtkTriangleFilter* triangleFilter = vtkTriangleFilter::New();
    //#triangleFilter->SetInputData(polyData);
    //#triangleFilter->Update();

    //#vtkPolyData* polyData2 = triangleFilter->GetOutput();

    vtkPoints* points = polyData->GetPoints();
    unsigned int nPoints = unsigned(polyData->GetNumberOfPoints());
	unsigned int nDim = 3;
	
    vtkDataArray* pointDataVtk = points->GetData();
    //std::cout<<"pointDataVtk is : " <<pointDataVtk->GetDataTypeAsString() <<", " <<pointDataVtk->GetDataTypeSize()*8 <<" bits" <<std::endl;
    float* pointDataC = static_cast<float*>(pointDataVtk->GetVoidPointer(0));
	matrix.set_size(nPoints,nDim);
    //std::cout<<"Printing " <<nPoints <<" vertices" <<std::endl;
	
	
	
    for (int i = 0;i < nPoints; i++) {
        //std::cout<<i <<": [ ";
        for (int j = 0; j < nDim; j++)
            //std::cout << pointDataC[i*3 + j] << " ";
			matrix(i,j) = pointDataC[i*nDim + j];
        //std::cout<<"]" <<std::endl;
    }
	return 1;
}

template<typename T>
void SaveMatrixToAsciiFile(const char * filename, const vnl_matrix<T>& x) {
  if (strlen(filename)>0) {
    std::ofstream outfile(filename,std::ios_base::out);
    x.print(outfile);
  }
}

template<typename T>
int SaveStlToFile(const char * filename, vnl_matrix<T>& matrix, vtkSmartPointer<vtkPolyData> polyData) {
	//Writes matrix point data to stl file.  Polydata is used to define the elements.
	//matrix must contain 3d point data and match the number of points in polyData

	//reintroduce matrix values into polydata to update the point positions - assuming that connectivity is already in polydata
	if (polyData.GetPointer() != NULL) {
	vtkPoints* points = polyData->GetPoints();
    unsigned int nPoints = unsigned(polyData->GetNumberOfPoints());
	unsigned int nDim = 3;
	
    vtkDataArray* pointDataVtk = points->GetData();
    //std::cout<<"pointDataVtk is : " <<pointDataVtk->GetDataTypeAsString() <<", " <<pointDataVtk->GetDataTypeSize()*8 <<" bits" <<std::endl;
    float* pointDataC = static_cast<float*>(pointDataVtk->GetVoidPointer(0));
	
    //std::cout<<"Printing " <<nPoints <<" vertices" <<std::endl;
	
	
	//overwriting point locations with locations from matrix
	
    for (int i = 0;i < nPoints; i++) {
        //std::cout<<i <<": [ "<< std::flush;
		for (int j = 0; j < nDim; j++) {
            //std::cout << pointDataC[i*3 + j] << " " << std::flush;
			pointDataC[i*nDim + j] = matrix(i,j);
		}
        //std::cout<<"]" <<std::endl;
    }


	vtkSmartPointer<vtkSTLWriter> writer = vtkSmartPointer<vtkSTLWriter>::New();
	writer->SetInputData(polyData);
    writer->SetFileName(filename);
    writer->Write();

    //#vtkTriangleFilter* triangleFilter = vtkTriangleFilter::New();
    //#triangleFilter->SetInputData(polyData);
    //#triangleFilter->Update();

    //#vtkPolyData* polyData2 = triangleFilter->GetOutput();

	}
	return 1;
	
}


template<typename T>
void SaveVectorToAsciiFile(const char * filename, const vnl_vector<T>& x) {
  if (strlen(filename)>0) {
    std::ofstream outfile(filename,std::ios_base::out);
    outfile << x;
  }
}

}  // namespace gmmreg

#endif // GMMREG_UTILS_IO_UTILS_H_
