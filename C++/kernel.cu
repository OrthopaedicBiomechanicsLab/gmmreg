#include <iostream>
#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <cublas_v2.h>
#include <curand.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include "kernel.h"
#include <thrust/reduce.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/detail/config.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/tuple.h>
#include <thrust/functional.h>
#include <device_functions.h>
#include "device_launch_parameters.h"



/*Observation - 
If you need to do more than one matrix multiplication in your code,
it is advisable to move the create/destroy handle code the main function, 
and use the same handle for all multiplications. 
*/

// Fill the array A(nr_rows_A, nr_cols_A) with random numbers on GPU
/*void GPU_fill_rand(double *A, int nr_rows_A, int nr_cols_A) {
	// Create a pseudo-random number generator
	curandGenerator_t prng;
	curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);

	// Set the seed for the random number generator using the system clock
	curandSetPseudoRandomGeneratorSeed(prng, (unsigned long long) clock());

	// Fill the array with random numbers on the device
	curandGenerateUniformDouble(prng, A, nr_rows_A * nr_cols_A);
}*/

__global__ void
computeP1Kernel(double* arrayX, double* arrayY, double* arrayP, int m, int s, int d, double k, double* d_column_sum, double* d_column_sum_array);

void computeP1KernelHelper(double *x, double *y, double* P, int m, int s, int d, double k, double* column_sum, double* column_sum_array){

	double *d_x, *d_y, *d_P, *d_column_sum, *d_column_sum_array;

	cudaMallocManaged(&d_x, m*d*sizeof(double));
	cudaMallocManaged(&d_y, s*d*sizeof(double));
	cudaMallocManaged(&d_P, m*s*sizeof(double));
	cudaMallocManaged(&d_column_sum, s*sizeof(double));
	cudaMallocManaged(&d_column_sum_array, m*sizeof(double));

	cudaMemcpy(d_x, x, m*d* sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_y, y, s*d* sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_P, P, m*s* sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_column_sum, column_sum, s* sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_column_sum_array, column_sum_array, m * sizeof(double), cudaMemcpyHostToDevice);
	std::cout << "x[0] after switch: " << x[0] << std::endl;

	int blockSize = 256;
	int numBlocks = (s + blockSize - 1) / blockSize;
	computeP1Kernel << <numBlocks, blockSize >> >(d_x, d_y, d_P, m, s, d, k, d_column_sum, d_column_sum_array);
	//wait for gpu to finish
	cudaDeviceSynchronize();

	cudaMemcpy(P, d_P, m*s * sizeof(double), cudaMemcpyDeviceToHost);
	cudaMemcpy(column_sum, d_column_sum, s * sizeof(double), cudaMemcpyDeviceToHost);


	//cudaMemcpy(h_test, d_test, m*d * sizeof(double), cudaMemcpyDeviceToHost);
	//std::cout << h_test[0] << " " << h_test[1] << " " << h_test[2] << " " << h_test[3] << " " << h_test[4] << " " << h_test[5] << std::endl;
	//std::cout << column_sum_array[0] << " " << column_sum_array[1] << " " << column_sum_array[2] << " " << column_sum[3];


	cudaFree(d_x);
	cudaFree(d_y);
	cudaFree(d_P);
	cudaFree(d_column_sum);
	cudaFree(d_column_sum_array);

}

__global__
void computeP1Kernel(double* d_x, double* d_y, double* d_P, int m, int s, int d, double k, double* d_column_sum, double* d_column_sum_array) {
	//int j = threadIdx.x + blockDim.x*blockIdx.x;
	//if (j < s)

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	//for (int j = 0; j < s; ++j)
	for (int j = index; j < s; j += stride)
	{
		double column_sumElement = 0;
		{
			//
			for (int i = 0; i < m; ++i)
			{
				double* rArray = new double[d];
				for (int t = 0; t < d; ++t)
				{
					rArray[t] = (d_x[i*d + t] - d_y[j*d + t])*(d_x[i*d + t] - d_y[j*d + t]);
				}
				double rSum = thrust::reduce(thrust::seq, rArray, rArray + d);
				free(rArray);
				//double rSum = 0;
				d_P[i*s + j] = exp(rSum / k);
				d_column_sum_array[i] = rSum / k;
			}

			double column_sumElement = thrust::reduce(thrust::seq, d_column_sum_array, d_column_sum_array + m);
			//double column_sumElement = 0;
			d_column_sum[j] = column_sumElement;
		}
	}
}


// Multiply the arrays A and B on GPU and save the result in C
// C(m,n) = A(m,k) * B(k,n)


void gpu_blas_mmul(double *A, double *B, double*C, int m, int n, int k) {
	int hA = m, wA = n, wB = k;
	//hA = rows_A, wA = cols_A, wB = cols_B
	/*const float alf = 1;
	const float bet = 0;
	const float *alpha = &alf;
	const float *beta = &bet;
	*/
	const double alpha = 1;
	const double beta = 0;

	////*begin: additions from main*////

	//allocating 3 arrays on GPU 
	double *d_A, *d_B, *d_C;
	cudaMallocManaged(&d_A, m * n * sizeof(double));
	cudaMallocManaged(&d_B, n * k * sizeof(double));
	cudaMallocManaged(&d_C, m * k * sizeof(double));

	//transfer arrays
	cudaMemcpy(d_A, A, m * n * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(d_B, B, n * k * sizeof(double), cudaMemcpyHostToDevice);


	////*end: additions from main*////

	// Multiply A and B on GPU
	// Create a handle for CUBLAS
	cublasHandle_t handle;
	cublasCreate(&handle);

	// Do the actual multiplication
	//cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, m, n, k, &alpha, BT, ldb, AT, lda, &beta, CT, ldc);
	//k is cols_b, m is rows_A, n = cols_a //int lda = m (hA), ldb = k (wB), ldc = n (wA);
	//k is cols_b, m is rows_A, n = cols_a //int lda = m (hA), ldb = k (wB), ldc = n (wA);
	cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, wB, hA, wA, &alpha, d_B, wB, d_A, wA, &beta, d_C, wB);
	cudaStreamSynchronize(0);
	// Destroy the handle
	cublasDestroy(handle);

	//Copy the result on host memory
	cudaMemcpy(C, d_C, m * k * sizeof(double), cudaMemcpyDeviceToHost);

	//Free GPU memory
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);
}


//Print matrix A(nr_rows_A, nr_cols_A) storage in column-major format
void print_matrix(const double *A, int nr_rows_A, int nr_cols_A) {

	for (int i = 0; i < nr_rows_A; i++){
		for (int j = 0; j < nr_cols_A; j++){
			std::cout << A[j * nr_rows_A + i] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void print_row_matrix(const double *A, int nr_rows_A, int nr_cols_A) {
	for (int i = 0; i < nr_rows_A; i++){
		for (int j = 0; j < nr_cols_A; j++){
			std::cout << A[i * nr_cols_A + j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}



/*
float* column_to_row_order(float *A, int nr_rows, int nr_cols){
	float *B = new float[nr_rows*nr_cols];

	for (int = 0; i < )
}*/

