#ifndef GMMREG_CPD_H_
#define GMMREG_CPD_H_

#include "gmmreg_base.h"

namespace gmmreg {

class CoherentPointDrift : public Base {
 public:
  CoherentPointDrift(): eps_(0.0000000001) {
    strcpy(section_, "GMMREG_EM");
  }
  virtual ~CoherentPointDrift() {}

 private:
	 void StartRegistration(vnl_vector<InternalCoordinateComponentType>&) override;
  void SetParam(vnl_vector<InternalCoordinateComponentType>&);
  int SetInitParams(const char*) override;
  void SaveResults(const char*, const vnl_vector<InternalCoordinateComponentType>&) override;
  int PrepareInput(const char*) override;
  void PrepareParamGradient(bool);
  void PerformTransform(const vnl_vector<InternalCoordinateComponentType>&) override;
  double BendingEnergy() override;
  void ComputeGradient(const InternalCoordinateComponentType lambda,
	  const vnl_matrix<InternalCoordinateComponentType>& gradient,
      vnl_matrix<double>& grad_all) override {};
  void PrepareOwnOptions(const char*) override;
  virtual void PrepareBasisKernel() = 0;
  virtual double UpdateParam() = 0;

 protected:
	 vnl_matrix<InternalCoordinateComponentType> basis_, param_all_;
	 InternalCoordinateComponentType EMtol, tol, beta_, anneal;
  int max_iter, max_em_iter, outliers;
  //vnl_vector<double> column_sum;
  //double outlier_term;
  vnl_matrix<InternalCoordinateComponentType> P;
  double eps_;
};

class CoherentPointDriftTps: public CoherentPointDrift {
 private:
	 vnl_matrix<InternalCoordinateComponentType> tps_;
	 vnl_matrix<InternalCoordinateComponentType> affine_;
	 vnl_matrix<InternalCoordinateComponentType> nP;
	 vnl_matrix<InternalCoordinateComponentType> G, Q1, Q2, R, invR, invG;

  void PrepareBasisKernel();
  double UpdateParam();
};

class CoherentPointDriftGrbf: public CoherentPointDrift {
 private:
  vnl_matrix<InternalCoordinateComponentType> dPG;
  vnl_matrix<InternalCoordinateComponentType> dPY0;
  vnl_matrix<InternalCoordinateComponentType> Gtranspose, invG;

  void PrepareBasisKernel();
  double UpdateParam();
};

}  // namespace gmmreg

#endif  // GMMREG_CPD_H_
