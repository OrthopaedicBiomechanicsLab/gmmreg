# gmmreg
=========

Original code written by Bing Jian.
Slight modifications made to make python installation easier.

Python Installation Instructions:
```shell
cd Python
python setup.py install
```

How to check if its working:
```shell
cd data
```

Now launch a python interpreter and exectute the following code:
```python
import gmmreg
gmmreg.test('fish_partial.ini')
```


Implementations of the robust point set registration algorithm described in the paper "Robust Point Set Registration Using Gaussian Mixture Models", Bing Jian and Baba C. Vemuri, IEEE Transactions on Pattern Analysis and Machine Intelligence, 2011, 33(8), pp. 1633-1645.
